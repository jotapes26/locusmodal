/*
    # Classe Elemento
    
    Classe criada com o intuido de ajudar a criacao de elementos HTML.
    Algumas propriedades podem ser definidas atraves do parametro propsObject.
    Para a utilização da classe, necessário ter o jQuery.

    @params
        - element:string (obrigatorio): Nome do elemento HTML que deseja criar
        - propsObject:object (opcional): objeto com atributos que vao ser adicionados ao elemento criado
*/
function Elemento(element, propsObject) {
    'use strict';

    if (typeof jQuery == "undefined") {
        throw new Error("Elemento JavaScript method requires jQuery");
    }

    if (!element || element === '') {
        throw new Error('Set element param');
    }

    var elem = new $('<'+element+'></'+element+'>');

    if (propsObject) {
        if (typeof propsObject !== 'object' && !Array.isArray(propsObject) && Object.keys(propsObject).length < 1) {
            throw new Error('PropObject param empty');
        }

        if (propsObject.id) {
            elem.attr('id', propsObject.id);
        }
    
        if (propsObject.styles) {
            elem.attr('style', propsObject.styles);
        }
    
        if (propsObject.classes) {
            elem.addClass(propsObject.classes);
        }

        if (propsObject.css) {
            Object.keys(propsObject.css).map(function(attr) {
                elem.css(attr, propsObject.css[attr]);
            });
        }
    
        if (propsObject.text) {
            elem.text(propsObject.text);
        }
    
        if (propsObject.html) {
            elem.html(propsObject.html);
        }
    
        if (propsObject.datas && propsObject.datas.length) {
            propsObject.datas.map(function(data) {
                elem.data(data.key, data.value);
            });
        }
    }

    return elem;
}

/*
    # Classe Modal

    Esta classe foi criada para o uso de modais no sistema.
    Para a utilização da classe, necessário ter o jQuery e o Bootstrap.

    @params
        - initObject:string||object (obrigatorio): Parametro que pode ser uma string ou objeto.
        - message:string (opcional): Mensagem que vai ser mostrada no body do modal.
        - closeButton: boolean (opcional): Parametro para exibir botao de fechar no canto superior direito, para fechar o modal.

*/
function Modal(initObject, message, closeButton) {
    'use strict';

    if (typeof jQuery == "undefined") {
        throw new Error("Modal JavaScript Class requires jQuery");
    }

    var self = this; // Isso serve para chamar o "this" em function com contexto proprio

    /* PROPRIEDADES */
    this.constructorType = null;
    this.id = initObject.id || 'Modal'.concat(String(new Date().getTime())).concat('-').concat(String(Math.floor((Math.random() * 100) + 1)));
    this.loadOn = 'body' // Proprieade para definir onde o modal vai ser inserido no DOM. Por padrao, vai ser sempre inserido no inicio do elmento BODY.

    this.title = '';

    this.theme = null;

    this.content = {
        css: {
            margin: '15% auto',
            width: window.innerWidth < 600 ? (window.innerWidth - 20)+'px' : '600px',
            padding: '8px',
            'background-color': '#F3F3F3',
            'border-radius': '5px'
        }
    };

    this.header = {
        css: {
            display: 'table',
            width: '100%',
        }
    };

    this.body = {
        classes: 'panel panel-default',
        css: {
            padding: '8px',
            'margin-bottom': '8px'
        }
    };

    this.footer = {
        css: {
            display: 'flex',
            'justify-content': 'flex-end',
        }
    };

    this.buttons = [];
    this.closeButton = closeButton || false;

    /* CONSTANTS */
    this.DEFAULT_THEMES = {
        SUCCESS: {
            'border': '2px solid',
            'border-color': '#27A227',
        },
        WARNING: {
            'border': '2px solid',
            'border-color': '#C5C500',
        },
        DANGER: {
            'border': '2px solid',
            'border-color': '#a22727',
        }
    };
    this.DEFAULT_CLASS_NAME = 'generic-modal-class-constant-name';
    this.DEFAULT_BUTTONS = {
        OK: {
            id: this.id.concat('-BTN_OK'),
            text: 'OK',
            classes: 'btn btn-primary',
            css: {
                'margin-left': '8px'
            },
            onClick: function() {
                self.hide();
            }
        },
        CONFIRM: {
            id: this.id.concat('-BTN_CONFIRM'),
            text: 'Confirmar',
            classes: 'btn btn-success',
            css: {
                'margin-left': '8px'
            }
        },
        CANCEL: {
            id: this.id.concat('-BTN_CANCEL'),
            text: 'Cancelar',
            classes: 'btn btn-danger',
            css: {
                'margin-left': '8px'
            },
            onClick: function() {
                self.hide();
            }
        },
        CLOSE: {
            id: this.id.concat('-BTN_CLOSE'),
            css: {
                float: 'right',
                color: '#626262',
                "font-size": '24px'
            },
            onClick: function() {
                self.hide();
            }
        }
    };

    if (!initObject) {
        throw new Error('Set the params/object initial');
    }

    if (typeof initObject === 'string') {
        this.constructorType = 'string';
        this.title = initObject;
        this.body.text = message || ''
    }

    if (isObject(initObject)) {
        this.constructorType = 'object';
        var attrs = Object.keys(initObject);
        attrs.map(function(key) {
            // TODO: Fazer um metodo para atribuir o objeto sem sobrescrever o que ja existe
            if (isObject(self[key]) && isObject(initObject[key])) {
                self[key] = spreadObject(self[key], initObject[key]);
            } else {
                self[key] = initObject[key];
            }
        });
    }

    if (this.constructorType === null) {
        throw new Error('Set the params/object initial');
    }

    this.containerProps = {
        id: this.id,
        classes: this.DEFAULT_CLASS_NAME,
        css: {
            position: 'fixed',
            'background-color': 'rgba(0, 0, 0, 0.5)',
            width: '100%',
            height: '100%',
            'z-index': '1100',
            display: 'none'
        }
    };

    /* CRIANDO BASE DO MODAL */
    this.container = Elemento('div', this.containerProps);

    $(this.loadOn).prepend(this.container); // Adicionando base do modal ao DOM, no inicio do selector definido no no this.loadOn

    this.ref = function() {
        return $('#'+this.id);
    }

    /* ESCONDER TODOS OS MODAIS */
    this.hideAll = function() {
        $('.'+this.DEFAULT_CLASS_NAME).hide();
    }

    /* Ocultar modal */
    this.hide = function() {
        this.container.hide();
    }

    /* Metodo responsavel por renderizar/exibir o modal no navegador */
    this.show = function() {
        if (this.theme) {
            if (typeof this.theme === 'string') {
                var defaultTheme = this.DEFAULT_THEMES[this.theme.toUpperCase()]
                if (isObject(defaultTheme)) {
                    this.content.css = spreadObject(this.content.css, defaultTheme);
                } else {
                    console.warn('Modal theme not found');
                }
            }

            if (isObject(this.theme) && Object.keys(this.theme).length > 0) {
                this.content.css = spreadObject(this.content.css, this.theme);
            }
        }

        var content = Elemento('div', this.content);

        content.append(renderHeader());
        content.append(renderBody());
        content.append(renderFooter());

        this.container.html(content);

        /*
            - DEFININDO EVENTOS DOS BOTOES
            Eventos de um elemento, só podem ser adicionados depois que ele estiver inserido ao DOM.
        */
        if (this.buttons.length > 0) {
            this.buttons.map(function(buttonProps) {
                $('#'+buttonProps.id).click(buttonProps.onClick);
            });
        }

        if(this.closeButton) {
            var closeButtonProps = this.DEFAULT_BUTTONS.CLOSE
            $('#'+closeButtonProps.id).click(closeButtonProps.onClick);
        }

        if (this.buttons.length === 0 && !this.closeButton) {
            console.warn('Aviso: Não foi definido nenhum botão para fechar o modal!');
        }

        this.container.show(); // exibir modal
    }

    /* Metodo privado responsavel por renderizar o HEADER do modal */
    function renderHeader() {
        var header = Elemento('div', self.header);

        var title = Elemento('div', {
            text: self.title,
            css: {
                float: 'left',
                'font-size': '24px',
                'font-weight': 'bold'
            }
        });

        header.append(title);

        if (self.closeButton) {
            var closeButton = Elemento('a', self.DEFAULT_BUTTONS.CLOSE);
            var icone = Elemento('span', { classes: 'glyphicon glyphicon-remove'} );

            closeButton.append(icone);
            header.append(closeButton);
        }

        return header;
    }

    /* Metodo privado responsavel por renderizar o BODY do modal */
    function renderBody() {
        var body = Elemento('div', self.body);
        return body;
    }

    /* Metodo privado responsavel por renderizar o FOOTER do modal */
    function renderFooter() {
        var footer = Elemento('div', self.footer);

        if (self.buttons.length > 0) {
            self.buttons.map(function(buttonProps) {
                footer.append(Elemento('button', buttonProps))
            });
            return footer;
        }

        return null;
    }

    this.spreadObject = spreadObject // "Exportando" funcao para chamar nos testes unitarios
    function spreadObject(object, addObj) {
        try {
            const addObjKeys = Object.keys(addObj);
            addObjKeys.map(function(key) {
                if (object[key]) {
                    if (isObject(object[key]) && isObject(addObj[key])) {
                        object[key] = spreadObject(object[key], addObj[key]);
                    } else {
                        object[key] = addObj[key];
                    }
                } else {
                    object[key] = addObj[key];
                }
            });
        } catch(error) {
            console.log(error);
        }

        return object;
    }

    this.isObject = isObject; // "Exportando" funcao para chamar nos testes unitarios
    function isObject(obj) {
        return typeof obj === 'object' && !Array.isArray(obj);
    }
}