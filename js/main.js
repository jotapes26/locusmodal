$(document).ready(function() {
    $('#jp-modal-1').on('click', function() {
        // Instanciando um novo objeto da classe Modal
        var modal = new Modal('Titulo do modal', 'Mensagem de exemplo ', true);

        // renderizar o modal
        modal.show();

        // esconder o modal
        // modal.hide(); // Se não estiver comentado, após o modal ser exibido, ele irá ser fechado.
    })

    $('#jp-modal-2').on('click', function() {
        // Objeto que vai ser passado para o construtor da classe Modal
        var modalObjectConstrutor = {
            // Definindo um ID especifico para o modal
            id: 'modal4life',
            // Titulo do modal
            title: 'Modal a little bit',
            // Body do modal - nesse caso estamos passando um texto para o atrubuto text
            body: {
                text: 'Mesagem de exemplo do modal'
            }
        };

        // Instanciando objeto
        var modal = new Modal(modalObjectConstrutor);
        
        // Definindo botões utilizando os botões padrões da classe
        modal.buttons.push(modal.DEFAULT_BUTTONS.OK);
        modal.buttons.push(modal.DEFAULT_BUTTONS.CANCEL);
        
        // Botão customizado
        var customButton = {
            // ID do botão, no qual será registrado a ação de click
            id: modal.id.concat('-BTN_CUSTOM'),
            // Texto que será exibido no botão
            text: 'Custom',
            // Definindo classes CSS
            classes: 'btn btn-warning',
            // Definindo propriedades CSS
            css: {
                'margin-left': '8px'
            },
            // Método que vai ser executado quando o botão for clicado
            onClick: function() {
                modal.hide();
            }
        };
        
        // Adicionando botão customizado
        modal.buttons.push(customButton);
        
        // Renderizando modal
        modal.show();
    })

    $('#jp-modal-3').on('click', function() {
        // Instanciando objeto
        var modal = new Modal('E só aceita texto?');

        // Definindo botão de fechar modal
        modal.closeButton = true

        // Definindo thema do modal
        modal.theme = modal.DEFAULT_THEMES.SUCCESS;

        // criando o corpo do body do modal
        var htmlBody = Elemento('div', { // Método Elemento, usado para criar elementos HTML
            // Definindo propriedade CSS
            css: {
                padding: '8px',
                'background-color': '#262626' // Setndo cor de fundo do body
            }
        });

        var texto = Elemento('p', {
            text: 'Acesse o site da Locus',
            css: {
                'font-size': '26px',
                color: '#EEE'
            }
        });

        var link = Elemento('a', {
            text: 'Locus custom Software',
            classes: 'btn btn-info'
        });
        link.attr('href', 'http://locus.software/');

        htmlBody.append(texto);
        htmlBody.append(link);

        modal.body = {
            html: htmlBody,
        }

        // renderizar o modal
        modal.show();
    })

    $('#jp-modal-4').on('click', function() {
        var modalSuccess = new Modal('Titulo Sucesso', 'Mensagem do modal de sucesso', true);
        modalSuccess.theme = modalSuccess.DEFAULT_THEMES.SUCCESS;
        modalSuccess.show();
    })

    $('#jp-modal-5').on('click', function() {
        var modalWarning = new Modal('Titulo Aviso', 'Mensagem do modal de aviso', true);
        modalWarning.theme = modalWarning.DEFAULT_THEMES.WARNING;
        modalWarning.show();
    })

    $('#jp-modal-6').on('click', function() {
        var modalDanger = new Modal('Titulo Erro', 'Mensagem do modal de erro', true);
        modalDanger.theme = modalDanger.DEFAULT_THEMES.DANGER;
        modalDanger.show();
    })

    $('#jp-modal-7').on('click', function() {
        var modal = new Modal('Titulo modal dos botões', 'Abaixo, os temas padrões dos botões da classe', true);

        modal.buttons.push(modal.DEFAULT_BUTTONS.OK);
        modal.buttons.push(modal.DEFAULT_BUTTONS.CONFIRM);
        modal.buttons.push(modal.DEFAULT_BUTTONS.CANCEL);

        modal.show();
    })

    $('#jp-modal-8').on('click', function() {
        var modal = new Modal('Sobrescrevendo botões', 'O botão CONFIRMAR abaixo, teve o método de click sobrescrito');

        modal.DEFAULT_BUTTONS.CONFIRM.onClick = function() {
            console.log('Sucesso!');
            modal.hide();
        }

        modal.buttons.push(modal.DEFAULT_BUTTONS.CONFIRM);

        modal.show();
    })
})